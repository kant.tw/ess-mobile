$(function() {

	//open footer follow
	$('.footer-follow').on('click', function(e) {
		e.preventDefault();
		$('.footerSocial').toggleClass('opened');
	});

	//offCanvas in header and footer
	var offCanvas = {
		init: function() {
			$('.mainNav').find('.subNav').each(function() {
				$(this).data('menuheight', $(this).height());
				$(this).height(0);
				$(this).css({opacity: 1});
			});
			this.bindEvents();
		}, 

		accTrigger: function() {
			$('.js-acchead').on('click', function(e) {
				e.preventDefault();
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).next('.subNav').removeClass('active').animate({height: 0}, 600, 'easeInOutQuad');
				} else {
					var targetHeight = $(this).next('.subNav').data('menuheight');
					$(this).addClass('active');
					$(this).next('.subNav').addClass('active').animate({height: targetHeight}, 600, 'easeInOutQuad');
					//close others
					var siblings = $(this).parent('li').siblings().find('.js-acchead');
					// console.log('test siblings: ' + siblings.length);
					siblings.removeClass('active');
					siblings.next('.subNav').removeClass('active').animate({height: 0}, 600, 'easeInOutQuad');
				}
				
			});
		},

		toggleTrigger: function() {
			$('.headerBottom .menu').on('click', function(e) {
				e.preventDefault();
				if ($('body').hasClass('menuOpened')) {
					$('body').removeClass('menuOpened openPageCover');
				} else {
					$('body').addClass('menuOpened openPageCover').removeClass('faqOpened');
					$('.headerBottom_search').removeClass('opened');
				}
			});

			$('.footer-faq').on('click', function(e) {
				e.preventDefault();
				if ($('body').hasClass('faqOpened')) {
					$('body').removeClass('faqOpened openPageCover');
				} else {
					$('body').addClass('faqOpened').removeClass('menuOpened');
				}
			});

			$('.headerBottom .search').on('click', function(e) {
				e.preventDefault();
				var searchWrap = $('.headerBottom_search');
				if (searchWrap.hasClass('opened')) {
					searchWrap.removeClass('opened');
				} else {
					searchWrap.addClass('opened');
					$('body').removeClass('menuOpened openPageCover');
				}
				// $('.headerBottom_search').toggleClass('opened');
			});
		},

		closeTrigger: function() {
			$('.js-closeNav').on('click', function(e) {
				e.preventDefault();
				$('body').removeClass('menuOpened openPageCover');
			});
		},

		styleTrigger: function() {
			$('.subNav a').on('click', function(e) {
				$(this).toggleClass('active');
			});
		},

		bindEvents: function() {
			this.toggleTrigger();
			this.accTrigger();
			this.closeTrigger();
			this.styleTrigger();
		}
	}
	offCanvas.init();



});


//***************** tabs *****************//
var tabs = function(options) {

    var el = document.querySelector(options.el);
    var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
    var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
    var activeIndex = 0;
    var initCalled = false;

    var init = function() {
    	if (!initCalled) {
		    initCalled = true;
		    el.classList.remove('no-js');

		    //cleaning
		    tabNavigationLinks.forEach(function(elem, idx) {
		    	elem.classList.remove('is-active');
		    });
		    tabContentContainers.forEach(function(elem, idx) {
		    	elem.classList.remove('is-active');
		    });
		    //end cleaning

		    tabNavigationLinks[0].className += ' is-active';
		    for (var i = 0; i < tabNavigationLinks.length; i++) {
		    	var link = tabNavigationLinks[i];
		    	handleClick(link, i);
		    }
		    
		    tabContentContainers[0].className += ' is-active';
		}
    };

    var handleClick = function(link, index) {
    	link.addEventListener('click', function(e) {
		    e.preventDefault();
		    goToTab(index);
		});
    };

    var goToTab = function(index) {
    	if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
			tabNavigationLinks[activeIndex].classList.remove('is-active');
			tabNavigationLinks[index].classList.add('is-active');
			tabContentContainers[activeIndex].classList.remove('is-active');
			tabContentContainers[index].classList.add('is-active');
			activeIndex = index;
		}
    };

    return {
    	init: init,
    	goToTab: goToTab
    };

};

window.tabs = tabs;
//***************** end tabs *****************//

	

